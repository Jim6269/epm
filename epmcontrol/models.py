# -*- coding: utf-8 -*-
"""
    epm.models
    ~~~~~~~~~~

    DESCRIPTION

    :copyright: (c) 2016 by MATTHEW LEMON.
    :license: MIT, see LICENSE for more details.
"""
import json

from .utils import number_of_rags, ready_graph_context_for_json, rag_labels
from django.db import models


# Create your models here.

class Portfolio(models.Model):
    """
    EPM Portfolio model
    """
    name = models.CharField(max_length=30)

    @property
    def greens(self):
        return number_of_rags(self.name, "GREEN")

    @property
    def amber_greens(self):
        return number_of_rags(self.name, "AMBER/GREEN")

    @property
    def ambers(self):
        return number_of_rags(self.name, "AMBER")

    @property
    def amber_reds(self):
        return number_of_rags(self.name, "AMBER/RED")

    @property
    def reds(self):
        return number_of_rags(self.name, "RED")

    def __str__(self):
        return self.name


class Role(models.Model):
    """
    EPM Role model
    """
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Person(models.Model):
    """
    EPM Person model
    """
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=30)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)

    def _get_full_name(self):
        "Returns a person's full name."
        return '%s %s' % (self.first_name, self.last_name)

    def _get_project(self):
        "Returns the project someone is on."
        project = self.project_set.all()[0]
        return project

    full_name = property(_get_full_name)
    project = property(_get_project)

    def __str__(self):
        return self.first_name + " " + self.last_name

    class Meta:
        verbose_name_plural = "People"


class RAG(models.Model):
    """
    EPM RAG model
    """
    rag = models.CharField(max_length=12)

    def __str__(self):
        return self.rag

    class Meta:
        verbose_name = "RAG"
        verbose_name_plural = "RAG ratings"


class Project(models.Model):
    """
    EPM Project model
    """
    name = models.CharField(max_length=200)
    sro_rating = models.ForeignKey(RAG, on_delete=models.CASCADE, verbose_name="SRO RAG Rating")
    sro = models.ForeignKey(Person, on_delete=models.CASCADE, null=True, verbose_name="SRO")
    portfolios = models.ManyToManyField(Portfolio, related_name='projects', verbose_name="Portfolio")

    def __str__(self):
        return self.name


class Quarter(models.Model):
    """
    EPM Quarter model
    """
    qref = models.CharField(max_length=1, verbose_name='Quarter')
    yearref = models.CharField(max_length=4, verbose_name="Year")
    start = models.DateField()
    end = models.DateField()

    def get_label(self, export_format=None):
        """
        Provides a handy label for the quarter (e.g. "Q1/2012")
        :param export_format: pass 'graphs' if you want labels to be exported
        as a dictionary in the form {'label': 'q12012'} which is used for later
        json conversion in the view, for passing to Javascript functions.
        :type export_format: string
        :return: string version of the object identifier
        """
        if export_format == 'graphs':
            return dict(label="q{0}{1}".format(self.qref, self.yearref))
        else:
            return "Q{0}/{1}".format(self.qref, self.yearref)

    @property
    def bicc_returns(self):
        """
        Get a QuerySet of BICCReturn objects for a Quarter. Call count() method
        on it to get number of returns, etc.
        :return: QuerySet of BICCReturn objects.
        """
        return self.biccreturn_set.all()

    def get_rag_count(self, jsonify=False, r_labels=False):
        """
        Get a count of each RAG rating for a BICCReturn object, e.g. {'AMBER':10 ,
        'GREEN': 8} and so on.
        :param: jsonify if True, will jsonify the resulting dict for passing to a template.
        False will return a simple dict.
        :return: dictionary of rag : count pairs
        """
        bicc_returns = self.biccreturn_set.all()
        rags = [r.sro_confidence_rag.rag for r in bicc_returns]
        from collections import Counter
        count_rags = Counter(rags)
        if jsonify:
            if r_labels:
                labels = rag_labels()
                context = ready_graph_context_for_json(count_rags, self.get_label(export_format='graphs'))
                context.update(labels)
                return context
            else:
                return json.dumps(
                    ready_graph_context_for_json(count_rags, self.get_label(export_format='graphs'))
                )
        else:
            return dict(count_rags)

    def __str__(self):
        return "Q%s/%s" % (self.qref, self.yearref)


class BICCReturn(models.Model):
    """
    EPM BICC Return model
    """
    project = models.ForeignKey(Project, related_name='bicc_return')
    quarter = models.ForeignKey(Quarter)
    sro_confidence_rag = models.ForeignKey(RAG, on_delete=models.CASCADE, verbose_name="SRO Confidence RAG")

    def __str__(self):
        return "%s : %s" % (self.quarter, self.project)

    # TODO probably needs a method for generating a json representation

    class Meta:
        verbose_name = "BICC Return"
        verbose_name_plural = "BICC Returns"
