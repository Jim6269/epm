from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^projects/(?P<id>[0-9]+)/$', views.single_project, name='single_project'),
    url(r'^projects/$', views.projects, name='projects'),
    url(r'^bicc_returns/(?P<year>[0-9]+)/(?P<quarter>[0-9]+)/$', views.bicc_returns_per_quarter,
        name='bicc_returns_per_quarter'),
    url(r'^graphic/1$', views.aggregate_returns_rag_per_quarter_single, name='aggregrate_graph'),
    url(r'^graphic/multi/1$', views.aggregate_returns_rag_per_quarter, name='aggregrate_graph_multi'),
]