def number_of_rags(portfolio, rag_type):
    """
    Calculate the number of projects rated as a particular RAG within a portfolio.
    :param portfolio:
    :param rag_type:
    :return: integer
    """
    from .models import Project
    return len(Project.objects.filter(portfolios__name=portfolio, sro_rating__rag=rag_type))


def rag_labels():
    """
    Function to return RAG labels for use in graphs or wherever.
    :return: dictionary of rag labels
    """
    from .models import RAG
    green_label = RAG.objects.filter(rag='GREEN')[0].rag
    amber_green_label = RAG.objects.filter(rag='AMBER/GREEN')[0].rag
    amber_label = RAG.objects.filter(rag='AMBER')[0].rag
    amber_red_label = RAG.objects.filter(rag='AMBER/RED')[0].rag
    red_label = RAG.objects.filter(rag='RED')[0].rag
    d = dict(
        green_label=green_label,
        amber_green_label=amber_green_label,
        amber_label=amber_label,
        amber_red_label=amber_red_label,
        red_label=red_label
        )
    return d


def quarter_data(quarter, year):
    """
    Return some basic data about a quarter.
    :param quarter:
    :param year:
    :return: q_data_label, q_data_q_data_quarter, q_data_year
    """
    from .models import Quarter
    q_data = Quarter.objects.filter(qref=str(quarter), yearref=str(year))[0]
    q_data_year = q_data.yearref
    q_data_quarter = q_data.qref
    q_data_label = "Q%s/%s" % (q_data_quarter, q_data_year)
    return q_data_label, q_data_quarter, q_data_year


def numbers_of_rags_for_each_quarter(b_returns):
    """
    Given a QuerySet of BICCReturns, returns a tuple of integers representing the
    number of each RAG rating in each BICCReturn.
    :param b_returns:
    :return: Count of amber_greens, amber_reds, ambers, greens, reds
    """
    greens = len(list(filter(lambda x: x.sro_confidence_rag_id == 1, b_returns)))
    amber_greens = len(list(filter(lambda x: x.sro_confidence_rag_id == 2, b_returns)))
    ambers = len(list(filter(lambda x: x.sro_confidence_rag_id == 3, b_returns)))
    amber_reds = len(list(filter(lambda x: x.sro_confidence_rag_id == 4, b_returns)))
    reds = len(list(filter(lambda x: x.sro_confidence_rag_id == 5, b_returns)))
    return amber_greens, amber_reds, ambers, greens, reds


def ready_graph_context_for_json(dictionary, label):
    """
    :param label: a string for the string representation of the quarter
    (e.g. 'q1/2012'
    :type dictionary: dict to be treated
    """
    d = {}
    for k, v in dictionary.items():
        k = k.replace("/", "_")
        d[k.lower() + '_' + label['label']] = v
    # include the original label dict in there too
    d.update(dictionary)
    return d


def all_quarter_data():
    """
    Return a dictionary containing lists of BICCReturns for each quarter.
    :return:  dictionary of BICCReturns whose key is the Quarter label
    """
    from .models import Quarter
    all_quarters = Quarter.objects.all()
    b_returns_per_quarter = [q.biccreturn_set.all() for q in all_quarters]
    b_list_per_quarter = []
    for b_return in b_returns_per_quarter:
        r_label = b_return[0].quarter.label
        b_list_per_quarter.append({r_label: b_return})
    return b_list_per_quarter
