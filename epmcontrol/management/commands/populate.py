"""
How to run this populate script properly:

- Delete all numbers .py files in migrations folder (leave __init__)
- Delete SQLite db file
- python manage.py makemigrations
- python manage.py migrate
- python manage.py createsuperuser
- python manage.py populate
You will then have a fresh database, migrations and set of data to work with. This will need to be
added to as development progresses.
"""
import random

from django.core.management.base import BaseCommand
from epmcontrol.models import Quarter, RAG, Role, Person, Portfolio, Project, BICCReturn

from datetime import date

class Command(BaseCommand):


    args = ''
    help = 'Populate the database with some initial data.'

    def _delete_roles(self):
        Role.objects.all().delete()

    def _create_roles(self):
        r1 = Role(name="Project Manager")
        r1.save()
        r2 = Role(name="Senior Responsible Officer (SRO)")
        r2.save()
        r3 = Role(name="Technical Manager")
        r3.save()
        r4 = Role(name="Team Leader")
        r4.save()
        r5 = Role(name="PMO Liaison")
        r5.save()

    def _delete_persons(self):
        Person.objects.all().delete()

    def _create_persons(self):
        first_names = [
            "Mark",
            "Colin",
            "Steve",
            "Martha",
            "Gemma",
            "Claire",
            "October",
            "Briony",
            "Spellton"
        ]
        last_names = [
            "Ceavis",
            "Helter",
            "Smeddum",
            "Smith-and-Wesson",
            "Terrence",
            "Goagles",
            "Heaslip-Carnivoria",
            "Philis-Tronk",
            "Jessles"
        ]
        for p in range(20):
            p = Person(
                first_name=random.choice(first_names),
                last_name=random.choice(last_names),
                role_id=random.choice([1, 2, 3, 4, 5]))
            p.save()

    def _delete_portfolios(self):
        Portfolio.objects.all().delete()

    def _create_portfolios(self):
        p1 = Portfolio(name="DfT Tier 1 Projects")
        p2 = Portfolio(name="DfT Tier 2 Projects")
        p1.save()
        p2.save()

    def _delete_rags(self):
        RAG.objects.all().delete()

    def _create_rags(self):
        green = RAG(rag="GREEN")
        amber_green = RAG(rag="AMBER/GREEN")
        amber = RAG(rag="AMBER")
        amber_red = RAG(rag="AMBER/RED")
        red = RAG(rag="RED")
        green.save()
        amber_green.save()
        amber.save()
        amber_red.save()
        red.save()

    def _delete_projects(self):
        Project.objects.all().delete()

    def _create_projects(self):
        project_random = [
            "Slipjet Construction Ltd",
            "Foster Homes Across Moldovia",
            "Ridding the World of Formula 3000",
            "Mark Stennford Hairdressing",
            "A12 Rounadabout - Make Them Hexagons",
            "Cumbrian Airways Air Crash Investigation",
            "Travelling Wilberries CDs - The Big Search",
            "Evian Is Found To Be Poison",
            "Eggard to Hemptoast Truckway",
            "Tarmacing the Southern Wastelands",
            "Shuttlecock Shuttlebuses Ltd",
            "Government Takeover - Lewisham Chipshops",
            "Bistel to Chipsked Dual Carriageway Extension",
            "Tempworth Trollypark and Buggy Storage",
            "Heathrow Extension",
            "Chopper Station on GMH"
        ]
        for p in range(40):
            portfolio = Portfolio.objects.get(name='DfT Tier 1 Projects')
            p = Project(
                name=random.choice(project_random),
                sro_rating_id=random.choice(range(1,6)),
                sro_id=random.choice(range(1,21)))
            p.save()
            p.portfolios.add(portfolio)


    def _delete_quarters(self):
        Quarter.objects.all().delete()

    def _create_quarters(self):
        f1 = Quarter(
            qref='1', yearref='2012', start=date(
                2012, 4, 1), end=date(
                2012, 6, 30))
        f2 = Quarter(
            qref='2', yearref='2012', start=date(
                2012, 7, 1), end=date(
                2012, 9, 30))
        f3 = Quarter(
            qref='3', yearref='2012', start=date(
                2012, 10, 1), end=date(
                2012, 12, 31))
        f4 = Quarter(
            qref='4', yearref='2012', start=date(
                2013, 1, 1), end=date(
                2013, 3, 31))
        f5 = Quarter(
            qref='1', yearref='2013', start=date(
                2013, 4, 1), end=date(
                2013, 6, 30))
        f6 = Quarter(
            qref='2', yearref='2013', start=date(
                2013, 7, 1), end=date(
                2013, 9, 30))
        f7 = Quarter(
            qref='3', yearref='2013', start=date(
                2013, 10, 1), end=date(
                2013, 12, 31))
        f8 = Quarter(
            qref='4', yearref='2013', start=date(
                2014, 1, 1), end=date(
                2014, 3, 31))
        f9 = Quarter(
            qref='1', yearref='2014', start=date(
                2014, 4, 1), end=date(
                2014, 6, 30))
        f10 = Quarter(
            qref='2', yearref='2014', start=date(
                2014, 7, 1), end=date(
                2014, 9, 30))
        f11 = Quarter(
            qref='3', yearref='2014', start=date(
                2014, 10, 1), end=date(
                2014, 12, 31))
        f12 = Quarter(
            qref='4', yearref='2014', start=date(
                2014, 1, 1), end=date(
                2015, 3, 31))
        f13 = Quarter(
            qref='1', yearref='2015', start=date(
                2015, 4, 1), end=date(
                2015, 6, 30))
        f14 = Quarter(
            qref='2', yearref='2015', start=date(
                2015, 7, 1), end=date(
                2015, 9, 30))
        f15 = Quarter(
            qref='3', yearref='2015', start=date(
                2015, 10, 1), end=date(
                2015, 12, 31))
        f16 = Quarter(
            qref='4', yearref='2015', start=date(
                2016, 1, 1), end=date(
                2016, 3, 31))
        f1.save()
        f2.save()
        f3.save()
        f4.save()
        f5.save()
        f6.save()
        f7.save()
        f8.save()
        f9.save()
        f10.save()
        f11.save()
        f12.save()
        f13.save()
        f14.save()
        f15.save()
        f16.save()

    def _delete_bicc_returns(self):
        BICCReturn.objects.all().delete()

    def _create_bicc_returns(self):
        all_quarters = Quarter.objects.all()
        all_projects = Project.objects.all()
        returns = [BICCReturn(project_id=project.id, quarter_id=quarter.id,
                              sro_confidence_rag_id=random.choice([1, 2, 3, 4, 5]))
                   for project in all_projects for quarter in all_quarters]
        for r in returns:
            r.save()

    def handle(self, *args, **options):
        self._delete_quarters()
        self._create_quarters()
        self._delete_portfolios()
        self._create_portfolios()
        self._delete_persons()
        self._create_persons()
        self._delete_roles()
        self._create_roles()
        self._delete_rags()
        self._create_rags()
        self._delete_projects()
        self._create_projects()
        self._delete_bicc_returns()
        self._create_bicc_returns()
