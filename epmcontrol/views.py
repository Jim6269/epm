import json

from django.shortcuts import render

from epmcontrol.utils import rag_labels, quarter_data, numbers_of_rags_for_each_quarter
from .models import Project, BICCReturn, Quarter


# Create your views here.
def index(request):
    pass


def projects(request):
    projs = Project.objects.all()
    context = {'project_list': projs}
    return render(request, 'epmcontrol/index.html', context)


def single_project(request, project_id):
    p = Project.objects.get(id=project_id)
    bicc_returns_l = p.bicc_return.all()
    context = {'project_data': p,
               'bicc_returns': bicc_returns_l}
    return render(request, 'epmcontrol/single_project.html', context)


def bicc_returns_per_quarter(request, year, quarter):
    """
    Objective is to delivery the JSON required to build THE graph to kick things off.
    This is a test function - we will refactor and reorganise once we prove the concept.
    :param request:
    :return:
    """
    # first thing is to get BICCReturns for a particular quarter
    b_returns = BICCReturn.objects.filter(quarter__yearref=int(year)).filter(quarter__qref=int(quarter))

    # then count them
    no_b_returns = len(b_returns)

    # if we want to have the data about the quarter, we can have that too
    q_data_label, q_data_quarter, q_data_year = quarter_data(quarter, year)

    # now we calculate the number of RAGs for each quarter
    amber_greens, amber_reds, ambers, greens, reds = numbers_of_rags_for_each_quarter(b_returns)

    labels_json = rag_labels()
    view_json = dict(
        q_data_label=q_data_label,
        q_data_year=q_data_year,
        q_data_quarter=q_data_quarter,
        no_b_returns=no_b_returns,
        greens=greens,
        amber_greens=amber_greens,
        ambers=ambers,
        amber_reds=amber_reds,
        reds=reds
    )

    view_json_to_go = json.dumps(view_json)
    view_labels_to_go = json.dumps(labels_json)

    context = {
        'q_data_label': q_data_label,
        'no_b_returns': [item for item in b_returns],
        'q_data_year': q_data_year,
        'q_data_quarter': q_data_quarter,
        'greens': greens,
        'amber_greens': amber_greens,
        'ambers': ambers,
        'amber_reds': amber_reds,
        'reds': reds,
        'view_json': view_json_to_go,
        'labels_json': view_labels_to_go
    }
    return render(request, 'epmcontrol/bicc_returns.html', context)


def aggregate_returns_rag_per_quarter_single(request):
    single_quarter = Quarter.objects.get(pk=3)
    json_export = single_quarter.get_rag_count(jsonify=True, r_labels=True)
    context = {
        'view_json': json_export
    }
    return render(request, 'epmcontrol/single_graph.html', context)


def aggregate_returns_rag_per_quarter(request):
    all_quarters = Quarter.objects.all()
    data_dict = json.dumps({q.get_label() : q.get_rag_count(jsonify=True, r_labels=True) for q in all_quarters})

    context = {
        'view_json': data_dict
    }
    return render(request, 'epmcontrol/single_graph.html', context)
