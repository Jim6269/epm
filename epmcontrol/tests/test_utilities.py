from django.test import TestCase

from epmcontrol.models import Project, RAG


class TestUtilities(TestCase):
    def setUp(self):
        self.project = Project(name="A1 Roundhouse", sro_rating=RAG(pk=1))
        self.rag = RAG(rag="GREEN")

    def test_new_project(self):
        self.assertEqual(self.project.name, "A1 Roundhouse")

    def test_create_new_rag(self):
        rag = RAG(rag="ORANGE")
        self.assertEqual(rag.rag, "ORANGE")

