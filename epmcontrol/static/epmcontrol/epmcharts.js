function aggregateRAGChartMulti() {
    "use strict";

    var green_label = viewJSON['green_label'];
    var amber_green_label = viewJSON['amber_green_label'];
    var amber_label = viewJSON['amber_label'];
    var amber_red_label = viewJSON['amber_red_label'];
    var red_label = viewJSON['red_label'];

    $('#agg_rag_multi').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Aggregate RAG scores for projects per quarter'
        },
        xAxis: {
            categories: [Object.keys(viewJSON)[0], Object.keys(viewJSON)[1], Object.keys(viewJSON)[2], Object.keys(viewJSON)[3]]
        },
        colors: [
            '#00CC25',
            '#AACC00',
            '#EBD009',
            '#EBA309',
            '#EB1509'
        ],
        yAxis: {
            min: 0,
            title: {
                text: 'Number of Project Returns'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -40,
            verticalAlign: 'bottom',
            y: 20,
            floating: false,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black'
                    }
                }
            }
        },
        series: [{
            name: viewJSON['Q1/2015']['green_label'],
            data: [
                parseInt(viewJSON['Q1/2015']['GREEN']),
                parseInt(viewJSON['Q2/2015']['GREEN']),
                parseInt(viewJSON['Q3/2015']['GREEN']),
                parseInt(viewJSON['Q4/2015']['GREEN']),
            ]
        }, {
            name: viewJSON['Q1/2015']['amber_green_label'],
            data: [
                parseInt(viewJSON['Q1/2015']['AMBER/GREEN']),
                parseInt(viewJSON['Q2/2015']['AMBER/GREEN']),
                parseInt(viewJSON['Q3/2015']['AMBER/GREEN']),
                parseInt(viewJSON['Q4/2015']['AMBER/GREEN']),
            ]
        }, {
            name: viewJSON['Q1/2015']['amber_label'],
            data: [
                parseInt(viewJSON['Q1/2015']['AMBER']),
                parseInt(viewJSON['Q2/2015']['AMBER']),
                parseInt(viewJSON['Q3/2015']['AMBER']),
                parseInt(viewJSON['Q4/2015']['AMBER']),
            ]
        }, {
            name: viewJSON['Q1/2015']['amber_red_label'],
            data: [
                parseInt(viewJSON['Q1/2015']['AMBER/RED']),
                parseInt(viewJSON['Q2/2015']['AMBER/RED']),
                parseInt(viewJSON['Q3/2015']['AMBER/RED']),
                parseInt(viewJSON['Q4/2015']['AMBER/RED']),
            ]
        }, {
            name: viewJSON['Q1/2015']['red_label'],
            data: [
                parseInt(viewJSON['Q1/2015']['RED']),
                parseInt(viewJSON['Q2/2015']['RED']),
                parseInt(viewJSON['Q3/2015']['RED']),
                parseInt(viewJSON['Q4/2015']['RED']),
            ]
        }]
    });
}


function aggregateRAGChart() {
    "use strict";
    var green_label = viewJSON['green_label'];
    var amber_green_label = viewJSON['amber_green_label'];
    var amber_label = viewJSON['amber_label'];
    var amber_red_label = viewJSON['amber_red_label'];
    var red_label = viewJSON['red_label'];

    var greens = viewJSON['GREEN'];
    var amber_greens = viewJSON['AMBER/GREEN'];
    var ambers = viewJSON['AMBER'];
    var amber_reds = viewJSON['AMBER/RED'];
    var reds = viewJSON['RED'];
    
    var categories;
    categories = ['TEST DATA'];
    
    $('#agg-rag-per-quarter').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Aggregrate RAG scores per project per quarter'
        },
        xAxis: {
            categories: categories
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Percentage of Projects'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'percent'
            }
        },
        colors: [
            '#00CC25',
            '#AACC00',
            '#EBD009',
            '#EBA309',
            '#EB1509'
        ],
        series: [{
            name: green_label,
            data: [parseInt(greens)]
        }, {
            name: amber_green_label,
            data: [parseInt(amber_greens)]
        }, {
            name: amber_label,
            data: [parseInt(ambers)]
        }, {
            name: amber_red_label,
            data: [parseInt(amber_reds)]
        }, {
            name: red_label,
            data: [parseInt(reds)]
        }]
    });
}


function singleColumnAggRagQuarter() {
    "use strict";
    var q_data_label = viewJSON['q_data_label'];
    var green_label = viewLabels['green_label'];
    var greens = viewJSON['greens'];
    var amber_green_label = viewLabels['amber_green_label'];
    var amber_greens = viewJSON['amber_greens'];
    var ambers = viewJSON['ambers'];
    var amber_label = viewLabels['amber_label'];
    var amber_reds = viewJSON['amber_reds'];
    var amber_red_label = viewLabels['amber_red_label'];
    var reds = viewJSON['reds'];
    var red_label = viewLabels['red_label'];
    var categories;
    categories = [q_data_label];
    $('#agg-rag-per-quarter').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Aggregrate RAG scores per project per quarter'
        },
        xAxis: {
            categories: categories
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Percentage of Projects'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'percent'
            }
        },
        colors: [
            '#00CC25',
            '#AACC00',
            '#EBD009',
            '#EBA309',
            '#EB1509'
        ],
        series: [{
            name: green_label,
            data: [parseInt(greens)]
        }, {
            name: amber_green_label,
            data: [parseInt(amber_greens)]
        }, {
            name: amber_label,
            data: [parseInt(ambers)]
        }, {
            name: amber_red_label,
            data: [parseInt(amber_reds)]
        }, {
            name: red_label,
            data: [parseInt(reds)]
        }]
    });
}
