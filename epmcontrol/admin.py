from django.contrib import admin
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.contrib.auth.models import User, Group
from django.contrib.admin import AdminSite
from django.core import urlresolvers

# Register your models here.
from django.utils.html import format_html

from .models import Project, RAG, Person, Role, Portfolio
from .models import Quarter, BICCReturn

class EPMAdmin(AdminSite):
    site_header = "EPM Control"
    index_title = "System administration"

class CustomUserAdmin(UserAdmin):
    pass


class CustomGroupAdmin(GroupAdmin):
    pass

class ProjectAdmin(admin.ModelAdmin):
    fields = ['name', 'sro', 'sro_rating', 'portfolios']
    filter_horizontal = ['portfolios']
    list_display = ('name', 'sro', 'sro_rating')
    list_filter = ('sro_rating', 'portfolios')
    search_fields = ['name']



class PersonAdmin(admin.ModelAdmin):
    fields = ['first_name', 'last_name', 'role']
    # list_display = ['full_name', 'project', 'role'] # TODO - fix - somone can be involved in more than one project
    list_display = ['full_name', 'role']


class PortfolioAdmin(admin.ModelAdmin):
    list_display = ['name',
                    'greens',
                    'amber_greens',
                    'ambers',
                    'amber_reds',
                    'reds']

class QuarterAdmin(admin.ModelAdmin):
    list_filter = ['yearref']


class BICCReturnAdmin(admin.ModelAdmin):
    list_filter = ['quarter__yearref', 'quarter__qref']

# create our own admin class
admin_site = EPMAdmin(name='epmadmin')

admin_site.register(Project, ProjectAdmin)
admin_site.register(Person, PersonAdmin)
admin_site.register(Portfolio, PortfolioAdmin)
admin_site.register(RAG)
admin_site.register(Role)
admin_site.register(Quarter, QuarterAdmin)
admin_site.register(BICCReturn, BICCReturnAdmin)
admin_site.register(User, UserAdmin)
admin_site.register(Group, GroupAdmin)
