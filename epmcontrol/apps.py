from django.apps import AppConfig


class EpmcontrolConfig(AppConfig):
    name = 'epmcontrol'
    verbose_name = 'EPM Control'
